import sys
import numpy as np
from vispy import gloo, app

VERT_SHADER = """
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
out vec3 ourColor;
void main() {
  gl_Position = vec4(aPos.xyz, 1.0);
  ourColor = aColor;
}
"""

FRAG_SHADER = """
#version 330 core
in vec3 ourColor;
void main() {
  gl_FragColor = vec4(ourColor, 1.0f);
}
"""

class Canvas(app.Canvas):
  def __init__(self):
    app.Canvas.__init__(self, keys='interactive')
    self.program = gloo.Program(VERT_SHADER, FRAG_SHADER)

    vertices = np.array(
        [([ 0.5, -0.5, 0.0], [1.0, 0.0, 0.0]),
         ([-0.5, -0.5, 0.0], [0.0, 1.0, 0.0]),
         ([ 0.0,  0.5, 0.0], [0.0, 0.0, 1.0])],
        dtype=[('aPos', np.float32, 3), ('aColor', np.float32, 3)])
    self.program.bind(gloo.VertexBuffer(vertices))

    self.show()

  def on_resize(self, event):
    width, height = event.size
    gloo.set_viewport(0, 0, width, height)

  def on_draw(self, event):
    gloo.clear((0.2, 0.3, 0.3, 1.0))
    indexes = np.array([0, 1, 2], dtype=np.uint32)
    self.program.draw('triangles', gloo.IndexBuffer(indexes))

if __name__ == '__main__':
  c = Canvas()
  if sys.flags.interactive != 1:
    app.run()
